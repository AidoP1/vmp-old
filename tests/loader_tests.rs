#![allow(dead_code)]
#![allow(unused_imports)]

use vmp::loader::load_vmp;
use std::path::Path;

#[cfg(feature = "vector2")]
#[test]
fn load_vmap_16x2() {
    use vmp::vector::Vector2;
    match load_vmp::<Vector2<u16>>(Path::new("tests/16x2.vmp")) {
        Err(err) => panic!("Load failed with error: {:?}", err),
        Ok(test_vmap) => assert_eq!(test_vmap.raw_data, vec![Vector2::new(47941, 65535)])
    };
}

#[cfg(feature = "vector2")]
#[test]
fn load_vmap_128x2() {
    use vmp::vector::Vector2;
    match load_vmp::<Vector2<u128>>(Path::new("tests/128x2.vmp")) {
        Err(err) => panic!("Load failed with error: {:?}", err),
        Ok(test_vmap) => assert_eq!(test_vmap.raw_data, vec![Vector2::new(248926341379189035064360629931988631949, 195786700775167410047374986636635418209)])
    };
}

#[cfg(feature = "vector2")]
#[test]
fn load_vmap_1x2() {
    use vmp::vector::Vector2;
    match load_vmp::<Vector2<bool>>(Path::new("tests/1x2.vmp")) {
        Err(err) => panic!("Load failed with error: {:?}", err),
        Ok(test_vmap) => assert_eq!(test_vmap.raw_data, vec![Vector2::new(true, false), Vector2::new(false, false), Vector2::new(true, true)])
    };
}

#[cfg(feature = "vector2")]
#[test]
fn load_vmap_2x2() {
    use vmp::vector::Vector2;

    // No 2bit primitive exists so create a struct that may be serialized
    #[derive(Debug)]
    #[derive(Clone)]
    #[derive(Copy)]
    #[derive(PartialEq)]
    pub enum Test2bit {
        One,
        Two,
        Three,
        Zero
    }

    use vmp::primitives::Packed;
    impl Packed for Test2bit {
        const PACKED_BITS: usize = 2;
        const PACKED_BYTES: usize = 1;

        fn unpack(raw_data: &[u8]) -> Self {
            match raw_data[0] {
                1 => Test2bit::One,
                2 => Test2bit::Two,
                3 => Test2bit::Three,
                0 => Test2bit::Zero,
                _ => panic!("Can't construct a Test2bit with this!")
            }
        }

        fn pack(self) -> Vec<u8> {
            vec![
            match self {
                Test2bit::One => 1,
                Test2bit::Two => 2,
                Test2bit::Three => 3,
                Test2bit::Zero => 0,
            }]
        }
    }

    match load_vmp::<Vector2<Test2bit>>(Path::new("tests/2x2.vmp")) {
        Err(err) => panic!("Load failed with error: {:?}", err),
        Ok(test_vmap) => assert_eq!(test_vmap.raw_data, vec![Vector2::new(Test2bit::Zero, Test2bit::Two), Vector2::new(Test2bit::One, Test2bit::Three), Vector2::new(Test2bit::Zero, Test2bit::Two)])
    };
}

#[cfg(feature = "colour4")]
#[test]
fn load_vmap_colour4() {
    use vmp::vector::Colour4;
    match load_vmp::<Colour4<u8>>(Path::new("tests/rgba.vmp")) {
        Err(err) => panic!("Load failed with error: {:?}", err),
        Ok(test_vmap) => assert_eq!(test_vmap.raw_data, vec![Colour4::new(19, 128, 56, 69), Colour4::new(0, 255, 0, 255), Colour4::new(0, 0, 255, 0), Colour4::new(26, 137, 92, 18)])
    };
}

#[test]
fn test_mask() {
    use vmp::Format;

    let one = Format::new_with_depth_bits(1, 1).unwrap();
    assert_eq!(one.depth_mask(), 0b1u8);

    let two = Format::new_with_depth_bits(2, 1).unwrap();
    assert_eq!(two.depth_mask(), 0b11u8);
    
    let four = Format::new_with_depth_bits(4, 1).unwrap();
    assert_eq!(four.depth_mask(), 0b1111u8);
    
    let eight = Format::new_with_depth_bits(8, 1).unwrap();
    assert_eq!(eight.depth_mask(), 0b11111111u8);
}
