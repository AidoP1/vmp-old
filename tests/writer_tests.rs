#![allow(dead_code)]
#![allow(unused_imports)]

use vmp::writer::write_vmp;
use vmp::loader::load_vmp;
use vmp::writer::WriteStatus;
use std::path::Path;

// TODO -> More tests

#[cfg(feature = "vector2")]
#[test]
fn full_rw_cycle_1x2() {
    use vmp::vector::Vector2;
    match load_vmp::<Vector2<bool>>(Path::new("tests/1x2.vmp")) {
        Err(err) => panic!("Load failed with error: {:?}", err),
        Ok(test_vmap) => {
            match write_vmp(Path::new("tests/w-1x2.vmp"), test_vmap) {
                WriteStatus::Success => assert_eq!(load_vmp::<Vector2<bool>>(Path::new("tests/1x2.vmp")).unwrap().raw_data, load_vmp::<Vector2<bool>>(Path::new("tests/w-1x2.vmp")).unwrap().raw_data),
                error => panic!("Error writing vmp: {:?}", error)
            }
        }
    }
}


#[cfg(feature = "vector2")]
#[test]
fn full_rw_cycle_16x2() {
    use vmp::vector::Vector2;
    match load_vmp::<Vector2<u16>>(Path::new("tests/16x2.vmp")) {
        Err(err) => panic!("Load failed with error: {:?}", err),
        Ok(test_vmap) => {
            match write_vmp(Path::new("tests/w-16x2.vmp"), test_vmap) {
                WriteStatus::Success => assert_eq!(load_vmp::<Vector2<u16>>(Path::new("tests/16x2.vmp")).unwrap().raw_data, load_vmp::<Vector2<u16>>(Path::new("tests/w-16x2.vmp")).unwrap().raw_data),
                error => panic!("Error writing vmp: {:?}", error)
            }
        }
    }
}
