// loader.rs
// The vector map loader allows reading in a vmap from a file

use std::path::Path;
use filebuffer::FileBuffer;

use crate::UsageIdentifier;
use crate::Flags;
use crate::Header;
use crate::Vector;
use crate::Format;
use crate::Version;
use crate::VectorMapSize;
use crate::VectorMap;

use crate::primitives::Packed;


/// Errors that may arise when reading a vmp
/// 
#[derive(Debug)]
#[derive(PartialEq)]
#[derive(Copy)]
#[derive(Clone)]
pub enum ReadError {
    FileOpen,
    NotVMP,
    UnsupportedVersion,
    Format,
    MissingData,
}

/// Load a vmp file into a VectorMap of the specified vector
/// This function will check that the vmp is compatible with your vector and its inner type
pub fn load_vmp<V: Vector>(path: &Path) -> Result<VectorMap<V>, ReadError> {
    // Load in the requested file
    let vmp_buffer = match FileBuffer::open(path) {
        Ok(buffer) => buffer,
        Err(_) => return Err(ReadError::FileOpen),
    };

    // The first 4 bytes are an identifier to verify that it is indeed a vmp file
    // The ASCII representation allows easy recognition in a text editor
    if *b"VMAP" != vmp_buffer[0..4] { return Err(ReadError::NotVMP) };

    // The next 4 bits is the vmp version
    let version = match Version::from_bits((vmp_buffer[4] & 0b11110000u8) >> 4) {
        Some(version) => version,
        // Only read versions understood by this loader. As the first revision this is only Version::Zero
        None => return Err(ReadError::UnsupportedVersion),
    };

    // The next 5 bits is the usage identifier
    let usage_identifier = UsageIdentifier::from_bits(((vmp_buffer[4] & 0b00001111u8) << 1) | ((vmp_buffer[5] & 0b10000000u8) >> 7));

    // The next 7 bits describe the format. 4 bits for depth (Represented as the exponent on 2) and 3 for the number of channels + 1
    let format = match Format::new(((vmp_buffer[5] as u8 & 0b01110000u8) >> 4) as usize, ((vmp_buffer[5] & 0b00000111u8) + 1) as usize) {
        Some(format) => format,
        None => return Err(ReadError::Format)
    };

    if let Some(expected_format) = Format::new_with_depth_bits(<V::Type as Packed>::PACKED_BITS, V::CHANNELS) {
        if expected_format != format {
            println!("Expected format: {:?} but got {:?}", expected_format, format);
            return Err(ReadError::Format);
        }
    };

    // The Size is the next 4 bytes but the flags are needed first to correctly interpret it
    // The next byte is the flags
    let flags = Flags::from_bits(vmp_buffer[10]);

    // The size must be serialized from a byte stream
    // TODO -> Maybe consider using the byteorder crate if endian-ness is an issue?
    let size = if flags.flat {
        VectorMapSize::Flat(((vmp_buffer[6] as u32) << 24) | ((vmp_buffer[7] as u32) << 16) | ((vmp_buffer[8] as u32) << 8) | (vmp_buffer[9] as u32))
    } else {
        VectorMapSize::Rect(((vmp_buffer[6] as u16) << 8) | (vmp_buffer[7] as u16), ((vmp_buffer[8] as u16) << 8) | (vmp_buffer[9] as u16))
    };

    // The start of the actual vector data may vary thanks to the optional hash
    let mut start_index = 11;
    // The last optional 64bits is an xxHash to verify the contents of the file
    let hash = if flags.hashed {
        start_index = 19;
        let hash = &vmp_buffer[11..19];
        // TODO -> Write a test for this algorithm and benchmark then implement it above if ok
        Some(hash.iter().enumerate().map(|(index, byte)| {
            // Each byte is shifted to the left by the correct number of bytes then summed
            (*byte as u64) << (64 - (index * 8))
        }).sum())
    } else {
        None
    };

    // Header parsing is done
    // Next is to unpack the raw vector data
    // TODO -> Split into packed / unpacked variants

    // Super fast integer division rounding up. Thank you u/HeroicKatora
    // https://www.reddit.com/r/rust/comments/bk7v15/my_next_favourite_way_to_divide_integers_rounding/
    fn div_up(lhs: usize, rhs: usize) -> usize {
        (0..lhs).step_by(rhs).size_hint().0
    }
    
    // Used on packed, unaligned vmp's
    let mut index = start_index * 8;
    let mut vmap_raw: Vec<V> = Vec::with_capacity(size.total() as usize);
    while index < (format.depth() * format.channels * size.total() as usize) + start_index * 8 {

        let unalligned_bits = &vmp_buffer[index / 8..(div_up(index + format.depth() * format.channels, 8))];
        // Only way to check buffer length to catch an error is to compare the slice length
        if unalligned_bits.len() != div_up(format.depth() * format.channels, 8) { return Err(ReadError::MissingData) };

        let mut raw_vector: Vec<u8> = Vec::with_capacity(unalligned_bits.len());

        // Split each byte into multiple bits
        for byte in unalligned_bits {
            if format.depth() < 8 {
                // This byte might overlap with one from the last vector
                let mut subbit_index = index % 8;
                while subbit_index < 8 {
                    raw_vector.push((byte >> ((8 - format.depth()) - subbit_index)) & format.depth_mask());
                    subbit_index += format.depth();
                }
            } else {
                raw_vector.push(*byte);
            }
        }

        let mut unpacked_types = Vec::with_capacity(V::CHANNELS);
        for alligned in raw_vector.chunks(<V::Type as Packed>::PACKED_BYTES) {
            unpacked_types.push(<V::Type as Packed>::unpack(alligned));
        }

        vmap_raw.push(V::construct(unpacked_types.as_slice()));

        index += format.depth() * format.channels;
    }

    Ok(VectorMap::from_vec(vmap_raw, Header::new(version, usage_identifier, format, size, flags, hash)))
}
