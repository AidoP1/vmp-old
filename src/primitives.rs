
/// Describes the packing of types
/// Although intended for primitives this may be easy serialization of any other type
pub trait Packed: Copy + Clone {
    const PACKED_BITS: usize;
    const PACKED_BYTES: usize;

    /// Unpack from a slice of bytes with bits set up to PACKED_BITS
    /// Ignore bits after this point
    fn unpack(raw_data: &[u8]) -> Self;

    //// Pack to aan array of bytes that is exactly PACKED_BITS long
    //// The remaining bits shall be ignored
    fn pack(self) -> Vec<u8>;
}

impl Packed for bool {
    const PACKED_BITS: usize = 1;
    const PACKED_BYTES: usize = 1;

    fn unpack(raw_data: &[u8]) -> Self {
        if raw_data[0] & 0b00000001u8 == 1 {
            true
        } else {
            false
        }
    }

    fn pack(self) -> Vec<u8> {
        vec![ if self {1} else {0} ]
    }
}

impl Packed for u8 {
    const PACKED_BITS: usize = 8;
    const PACKED_BYTES: usize = 1;

    fn unpack(raw_data: &[u8]) -> Self {
        raw_data[0]
    }

    fn pack(self) -> Vec<u8> {
        vec![self]
    }
}

impl Packed for i8 {
    const PACKED_BITS: usize = 8;
    const PACKED_BYTES: usize = 1;

    fn unpack(raw_data: &[u8]) -> Self {
        raw_data[0] as i8
    }

    fn pack(self) -> Vec<u8> {
        vec![self as u8]
    }
}

impl Packed for u16 {
    const PACKED_BITS: usize = 16;
    const PACKED_BYTES: usize = 2;

    fn unpack(raw_data: &[u8]) -> Self {
        ((raw_data[0] as u16) << 8) | raw_data[1] as u16
    }

    fn pack(self) -> Vec<u8> {
        vec![(self >> 8) as u8, self as u8]
    }
}

impl Packed for i16 {
    const PACKED_BITS: usize = 16;
    const PACKED_BYTES: usize = 2;

    fn unpack(raw_data: &[u8]) -> Self {
        (raw_data[0] as i16) << 8 |  raw_data[1] as i16
    }

    fn pack(self) -> Vec<u8> {
        vec![(self >> 8) as u8, self as u8]
    }
}

impl Packed for u128 {
    const PACKED_BITS: usize = 128;
    const PACKED_BYTES: usize = 16;

    fn unpack(raw_data: &[u8]) -> Self {
        // Say what you will but hard coding is faster
        ((raw_data[0] as u128) << 120) |
        ((raw_data[1] as u128) << 112) |
        ((raw_data[2] as u128) << 104) |
        ((raw_data[3] as u128) << 96) |
        ((raw_data[4] as u128) << 88) |
        ((raw_data[5] as u128) << 80) |
        ((raw_data[6] as u128) << 72) |
        ((raw_data[7] as u128) << 64) |
        ((raw_data[8] as u128) << 56) |
        ((raw_data[9] as u128) << 48) |
        ((raw_data[10] as u128) << 40) |
        ((raw_data[11] as u128) << 32) |
        ((raw_data[12] as u128) << 24) |
        ((raw_data[13] as u128) << 16) |
        ((raw_data[14] as u128) << 8) |
        raw_data[15] as u128
    }

    fn pack(self) -> Vec<u8> {
        vec![(self >> 120) as u8,
        (self >> 112) as u8,
        (self >> 104) as u8,
        (self >> 96) as u8,
        (self >> 88) as u8,
        (self >> 80) as u8,
        (self >> 72) as u8,
        (self >> 64) as u8,
        (self >> 56) as u8,
        (self >> 48) as u8,
        (self >> 40) as u8,
        (self >> 32) as u8,
        (self >> 24) as u8,
        (self >> 16) as u8,
        (self >> 8) as u8,
        self as u8]
    }
}

impl Packed for i128 {
    const PACKED_BITS: usize = 128;
    const PACKED_BYTES: usize = 16;

    fn unpack(raw_data: &[u8]) -> Self {
        ((raw_data[0] as i128) << 120) |
        ((raw_data[1] as i128) << 112) |
        ((raw_data[2] as i128) << 104) |
        ((raw_data[3] as i128) << 96) |
        ((raw_data[4] as i128) << 88) |
        ((raw_data[5] as i128) << 80) |
        ((raw_data[6] as i128) << 72) |
        ((raw_data[7] as i128) << 64) |
        ((raw_data[8] as i128) << 56) |
        ((raw_data[9] as i128) << 48) |
        ((raw_data[10] as i128) << 40) |
        ((raw_data[11] as i128) << 32) |
        ((raw_data[12] as i128) << 24) |
        ((raw_data[13] as i128) << 16) |
        ((raw_data[14] as i128) << 8) |
        raw_data[15] as i128
    }

    fn pack(self) -> Vec<u8> {
        vec![(self >> 120) as u8,
        (self >> 112) as u8,
        (self >> 104) as u8,
        (self >> 96) as u8,
        (self >> 88) as u8,
        (self >> 80) as u8,
        (self >> 72) as u8,
        (self >> 64) as u8,
        (self >> 56) as u8,
        (self >> 48) as u8,
        (self >> 40) as u8,
        (self >> 32) as u8,
        (self >> 24) as u8,
        (self >> 16) as u8,
        (self >> 8) as u8,
        self as u8]
    }
}