extern crate twox_hash;

extern crate filebuffer;

pub mod vector;
pub use vector::Vector;

pub mod primitives;
pub use primitives::Packed;

pub mod loader;
pub mod writer;

pub use loader::load_vmp;
pub use writer::write_vmp;

/// # Working with vmp's
/// 
/// **Loading from file:**
/// ```rust
/// use vmp::vector::Vector2;
/// use vmp::loader::load_vmp;
/// use std::path::Path;
///
/// // Will load a 2 dimensional vector populated with u16 types from file.vmp
/// let vector_map = load_vmp::<Vector2<u16>>(Path::new("file.vmp"));
/// ```
/// 
/// **Writing to files:**
/// ```rust
/// use vmp::writer::write_vmp;
/// use std::path::Path;
/// 
/// // create a vector map filled with zeroed Vector2's
/// let vector_map = VectorMap::filled(VectorMapSize::Flat());
/// 
/// // Will write the vector map to file.vmp based on the type of vector vector_map contains
/// write_vmp(Path::new("file.vmp"), vector_map);
/// ```


/// A contiguous collection of Vectors
/// Contains an unpacked Vector Map
pub struct VectorMap<V> where V: Vector {
    header: Header,
    pub raw_data: Vec<V>,
}

impl<V> VectorMap<V> where V: Vector {
    fn from_vec(raw_data: Vec<V>, header: Header) -> Self {
        Self {
            header,
            raw_data,
        }
    }

    /// Creates a new vmp filling it with the desired vector
    pub fn filled(size: VectorMapSize, usage: UsageIdentifier, flags: Flags, vector: V) -> Self {
        let format = Format::new_with_depth_bits(<V::Type as Packed>::PACKED_BITS, V::CHANNELS).expect("Vector of invalid type passed");
        Self {
            raw_data: {
                let mut raw_data = Vec::with_capacity(size.total() as usize * format.depth() * format.channels);
                for _ in 0..size.total() {
                    raw_data.push(vector)
                };
                raw_data
            },
            header: Header::new(Version::Zero, usage, format, size, flags, None),
        }
    }
}

impl<V> IntoIterator for VectorMap<V> where V: Vector {
    type Item = V;
    type IntoIter = VectorMapIter<V>;

    fn into_iter(self) -> Self::IntoIter {
        Self::IntoIter {
            index: 0,
            vector_map: self
        }
    }
}

pub struct VectorMapIter<V> where V: Vector {
    index: usize,
    vector_map: VectorMap<V>
}

impl<V> Iterator for VectorMapIter<V> where V: Vector {
    type Item = V;

    fn next(&mut self) -> Option<Self::Item> {
        if self.index >= self.vector_map.raw_data.len() { None }
        else {
            self.index += 1;
            Some(self.vector_map.raw_data[self.index - 1])
        }
    }
}

pub struct Header {
    version: Version,
    usage_identifier: UsageIdentifier,
    format: Format,
    size: VectorMapSize,
    flags: Flags,

    hash: Option<u64>,
}

impl Header {
    pub fn new(
        version: Version,
        usage_identifier: UsageIdentifier,
        format: Format,
        size: VectorMapSize,
        flags: Flags,
        hash: Option<u64>
    ) -> Self {
        Self {
            version,
            usage_identifier,
            format,
            size,
            flags,
            hash
        }
    }
}

pub struct Flags {
    hashed: bool,
    pallet: bool,
    flat: bool,
    unpacked: bool,
}

impl Flags {
    fn from_bits(raw_bits: u8) -> Self {
        macro_rules! bit_to_bool {
            ($bitflag:expr) => {
                if raw_bits & $bitflag == 0 { false } else { true };
            };
        }

        Self {
            hashed:     bit_to_bool!(0b10000000u8),
            pallet:     bit_to_bool!(0b01000000u8),
            flat:       bit_to_bool!(0b00100000u8),
            unpacked:   bit_to_bool!(0b00010000u8),
        }
    }

    fn to_bits(&self) -> u8 {
        macro_rules! bool_to_bit {
            ($boolean:tt << $bitflag:expr) => {
                if self.$boolean { 1 << $bitflag } else { 0 };
            };
        }

        bool_to_bit!(hashed     << 7) |
        bool_to_bit!(pallet     << 6) |
        bool_to_bit!(flat       << 5) |
        bool_to_bit!(unpacked   << 4)
    }
}

/// The 5bit usage identifier suggests how data should be interpreted
pub enum UsageIdentifier {
    Image,
    None,
    Unknown
}

impl UsageIdentifier {
    /// Get the UsageIdentifier based on its raw index
    /// Note: Only defined for the last 5 bits
    pub fn from_bits(raw_bits: u8) -> Self {
        match raw_bits {
            0 => UsageIdentifier::None,
            1 => UsageIdentifier::Image,
            _ => UsageIdentifier::Unknown
        }
    }

    /// Creates a 5 bit representation of the usage identifier to be stored on disk
    pub fn to_bits(&self) -> u8 {
        match self {
            UsageIdentifier::None | UsageIdentifier::Unknown => 0,
            Image => 1,
        }
    }
}

/// Represents the format of a VectorMap on disk
/// You should probably access this information directly from your vector,
/// if not note that `depth_exp` is the exponent (such that `depth = 2^(depth_exp)`) and not the depth itself
#[derive(Debug)]
#[derive(PartialEq)]
pub struct Format {
    depth_exp: usize,
    channels: usize,
}

impl Format {
    /// Creates a new format given the depth **exponent** such as is stored on disk
    /// It is likely that this is the unintended behaviour
    fn new(depth_exp: usize, channels: usize) -> Option<Self> {
        if depth_exp > 0b111usize || channels > 0b1111usize + 1 { None }
        else {Some(Self {depth_exp, channels})}
    }

    /// Create a format with depth in bits, not as an exponent. This is most likely the intended behaviour
    pub fn new_with_depth_bits(depth: usize, channels: usize) -> Option<Self> {
        Self::new(match depth {
            1   => 0,
            2   => 1,
            4   => 2,
            8   => 3,
            16  => 4,
            32  => 5,
            64  => 6,
            128 => 7,
            _ => return None
        }, channels)
    }

    /// Get the depth in bits
    #[inline(always)]
    pub fn depth(&self) -> usize {
        if self.depth_exp < 1 { 1 } else { 2 << (self.depth_exp - 1) } 
    }

    /// Calculates the bitmask of the depth to help discard packed bits
    pub fn depth_mask(&self) -> u8 {
        let mut mask = 0;
        for i in 0..if self.depth() < 8 { self.depth() } else { 8 } {
            mask |= 1 << i;
        };
        mask
    }

    /// Create a 7 bit representation of the format for direct storage in the header on disk
    pub fn to_bits(&self) -> u8 {
        ((self.depth_exp as u8) << 4) | ((self.channels - 1) as u8 & 0b00001111u8)
    }
}

/// The version of vmp being using. Older loaders will not load newer versions as the header is allowed to change past the first 36 bits in new versions
/// An enum is used for safety. The number is only valid to a size of 4 bits
pub enum Version {
    Zero,
}

impl Version {
    pub fn from_bits(raw_bits: u8) -> Option<Self> {
        match raw_bits {
            0 => Some(Version::Zero),
            _ => None
        }
    }

    pub fn to_bits(&self) -> u8 {
        match self {
            Zero => 0
        }
    }
}

/// Describes either of the sizes a VectorMap may have
pub enum VectorMapSize {
    Rect(u16, u16),
    Flat(u32),
}

impl VectorMapSize {
    /// Gets the total size:
    /// - The area of Rect
    /// - Or simply the value of Flat
    pub fn total(&self) -> u32 {
        match self {
            VectorMapSize::Rect(width, height) => (*width as u32) * (*height as u32),
            VectorMapSize::Flat(total) => *total,
        }
    }

    fn to_bits(&self) -> [u8; 4] {
        match self {
            VectorMapSize::Rect(width, height) => [(width >> 8) as u8, *width as u8 & 0b11111111u8, (height >> 8) as u8, *height as u8 & 0b11111111u8],
            VectorMapSize::Flat(size) => [(size >> 24) as u8, (size >> 16) as u8 & 0b11111111u8, (size >> 8) as u8 & 0b11111111u8, *size as u8 & 0b11111111u8]
        }
    }
}
