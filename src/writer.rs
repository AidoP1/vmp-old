// writer.rs
// The vector map loader writer allows writing packed vmp's of any valid format

use std::fs::File;
use std::path::Path;
use std::io::Write;

use crate::VectorMap;
use crate::vector::Vector;
use crate::primitives::Packed;

/// Describes possible outcomes of a write call
#[derive(Debug)]
pub enum WriteStatus {
    CreationError,
    FileWriteError,
    InvalidFormat,

    Success
}

/// Writes a VectorMap struct to the specified file
/// The Vector type should be inferred making this a simple call
pub fn write_vmp<V: Vector>(path: &Path, vector_map: VectorMap<V>) -> WriteStatus {
    let mut file = match File::create(path) {
        Err(_) => return WriteStatus::CreationError,
        Ok(file) => file,
    };

    // Write the vmp identifier
    if let Err(_) = file.write_all(b"VMAP") {
        return WriteStatus::FileWriteError
    };

    // TODO -> Include the optional hash
    let version: u8 = vector_map.header.version.to_bits();
    let usage_identifier: u8 = vector_map.header.usage_identifier.to_bits();
    let format: u8 = vector_map.header.format.to_bits();
    let size: [u8; 4] = vector_map.header.size.to_bits();
    let flags: u8 = vector_map.header.flags.to_bits();

    // Write the rest of the header
    if let Err(_) = file.write_all(&[
        ((version & 0b00001111u8) << 4) | ((usage_identifier & 0b00011110u8) >> 1),
        ((usage_identifier & 0b00000001u8) << 7) | (format & 0b01111111u8),
        size[0], size[1], size[2], size[3],
        flags
    ]) {
        return WriteStatus::FileWriteError
    };

    // Super fast integer division rounding up. Thank you u/HeroicKatora
    // https://www.reddit.com/r/rust/comments/bk7v15/my_next_favourite_way_to_divide_integers_rounding/
    fn div_up(lhs: usize, rhs: usize) -> usize {
        (0..lhs).step_by(rhs).size_hint().0
    }

    // Write the vector data
        
    let mut packed_data: Vec<u8> = Vec::with_capacity(div_up(vector_map.header.format.depth() * vector_map.header.format.channels * vector_map.header.size.total() as usize, 8));
    // Zero the Vec
    if vector_map.header.format.depth() < 8 {
        if <V::Type as Packed>::PACKED_BITS != vector_map.header.format.depth() { return WriteStatus::InvalidFormat };
        for _ in 0..div_up(vector_map.header.format.depth() * vector_map.header.format.channels * vector_map.header.size.total() as usize, 8) {
            packed_data.push(0)
        }
    };

    let mut index = 0;
    // Iterate over each vector then
    // * If the depth is < 8, pack the bits as densly as possible
    // * If the depth is > 8 it is known that the vector may simply be unpacked to an array of bits
    let format = vector_map.header.format;
    for vector in vector_map.raw_data.into_iter() {
        if format.depth() < 8 {
            for primitive in vector.deconstruct() {
                let byte = primitive.pack()[0];
                packed_data[index / 8] |= byte << 7 - (index % 8);
                index += format.depth();
            }

        } else {
            for primitive in vector.deconstruct() {
                for byte in primitive.pack() {
                    packed_data.push(byte)
                }
            }

            index += format.depth() * format.channels;
        }
    }

    if let Err(_) = file.write_all(packed_data.as_slice()) {
        return WriteStatus::FileWriteError
    };

    WriteStatus::Success
}