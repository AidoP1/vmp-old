// vector2.rs
// 2-dimensional mathematical vectors

use crate::vector::Vector;
use crate::primitives::Packed;

use std::ops::{ Add, Sub };

#[derive(Debug)]
#[derive(Clone)]
#[derive(Copy)]
pub struct Vector2<T> where T: Clone + Copy {
    pub x: T,
    pub y: T
}

impl<T> Vector2<T> where T: Clone + Copy {
    pub fn new(x: T, y: T) -> Self {
        Self {
            x: x,
            y: y
        }
    }
}

impl<T> Vector for Vector2<T> where T: Packed {
    const CHANNELS: usize = 2;
    type Type = T;

    fn construct(data: &[Self::Type]) -> Self {
        Self{
            x: data[0],
            y: data[1]
        }
    }

    fn deconstruct(self) -> Vec<Self::Type> {
        vec![self.x, self.y]
    }
}

impl<T: Eq> Eq for Vector2<T> where T: Clone + Copy {}

impl<T: PartialEq> PartialEq for Vector2<T> where T: Clone + Copy {
    fn eq(&self, other: &Self) -> bool {
        self.x == other.x && self.y == other.y
    }
}

impl<T: Add<Output = T>> Add for Vector2<T> where T: Clone + Copy {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y
        }
    }
}

impl<T: Sub<Output = T>> Sub for Vector2<T> where T: Clone + Copy {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        Self {
            x: self.x - other.x,
            y: self.y - other.y
        }
    }
}