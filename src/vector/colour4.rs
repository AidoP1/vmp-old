// colour4.rs
// A 4-dimensional mathematical vector intended to store an rgba colour

use crate::vector::Vector;
use crate::primitives::Packed;

#[derive(Debug)]
#[derive(Clone)]
#[derive(Copy)]
pub struct Colour4<T> where T: Clone + Copy {
    pub r: T,
    pub g: T,
    pub b: T,
    pub a: T
}

impl<T> Colour4<T> where T: Clone + Copy {
    pub fn new(r: T, g: T, b: T, a: T) -> Self {
        Self {
            r,g,b,a
        }
    }
}

impl<T> Vector for Colour4<T> where T: Packed {
    const CHANNELS: usize = 4;
    type Type = T;

    fn construct(data: &[Self::Type]) -> Self {
        Self{
            r: data[0],
            g: data[1],
            b: data[2],
            a: data[3]
        }
    }

    fn deconstruct(self) -> Vec<Self::Type> {
        vec![self.r, self.g, self.b, self.a]
    }
}

impl<T: Eq> Eq for Colour4<T> where T: Clone + Copy {}

impl<T: PartialEq> PartialEq for Colour4<T> where T: Clone + Copy {
    fn eq(&self, other: &Self) -> bool {
        self.r == other.r && self.g == other.g && self.b == other.b && self.a == other.a
    }
}