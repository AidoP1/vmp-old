// Lire - vector
// Mathematical vectors

extern crate num_traits;
use crate::primitives::Packed;

// Trait defining common vector methods
pub trait Vector: Copy + Clone {
    const CHANNELS: usize;
    type Type: Packed;

    fn construct(data: &[Self::Type]) -> Self;
    fn deconstruct(self) -> Vec<Self::Type>;
}

// TODO -> Look at making a macro for easy error free implementation of Vector

#[cfg(feature="vector2")]
pub mod vector2;
#[cfg(feature="vector2")]
pub use vector2::Vector2;

#[cfg(feature="colour4")]
pub mod colour4;
#[cfg(feature="colour4")]
pub use colour4::Colour4;