# vmp
![](https://gitlab.com/AidoP1/vmp-old/badges/master/build.svg)

Rust crate for reading and writing Vector Map (vmp) files.

Vmp is a file format for storing a contiguous array of mathematical vectors. The vector may have up to 16 dimensions (labelled channels) and numbers that are 2^(0..7) bits long on disk. The aim of vmp is to make storing arrays of ambiguous data on disk easy.

# Checklist
- [x] Basic implementation of loader and writer
- [ ] Document with code docs
- [ ] Write unit tests to cover everything
- [ ] Improve code safety
- [ ] Benchmark and look at areas for improvement
- [ ] External code review. Please submit all issues!
- [ ] Publish crate

# Usage

## Note. As of writing the crate is not published!

Add to your `Cargo.toml`:

```rust
[dependencies]
vmp = "0.0.2"
```

and to your `main.rs` or `lib.rs`:

```rust
extern crate vmp;
```

To load in a vmp file:

```rust
// Will load a 2 dimensional vector populated with u16 types from file.vmp
let vector_map = load_vmp::<Vector2<u16>>(Path::new("file.vmp"));
```

To write a VectorMap to a vmp file:

```rust
// Will write the vector map to file.vmp based on the type of vector vector_map contains
write_vmp(Path::new("file.vmp"), vector_map)
```